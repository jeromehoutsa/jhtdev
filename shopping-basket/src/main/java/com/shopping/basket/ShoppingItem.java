/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shopping.basket;

/**
 *
 * @author Jérôme HOUTSA
 */
public class ShoppingItem {
        
    private String name;
    private Double price;
    private Integer quantity;
    private Promo promo;
    
    public ShoppingItem(String name, Double price, Integer quantity){
        this.name=name;
        this.price=price;
        this.quantity=quantity;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    
    public Promo getPromo(){
        return promo;
    }
    public void setPromo(Promo promo){
        this.promo=promo;
    }
     
}
