/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shopping.basket;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jérôme HOUTSA
 */
public class ShoppingBasket {
    
    private final Map<String, ShoppingItem> basket=new HashMap<>();
    public Map<String,ShoppingItem> getBasket(){
        return basket;
    }  


    /**
     * Methode de calcul de la valeur du panier
     * @param basket
     * @return The final cost of the Basket
     */
    public Float calculeBasket(Map<String, ShoppingItem> basket){
        
        float sum=0;
        for (ShoppingItem item: basket.values()){
            if(item.getPromo()==null){
                //R.G. Default
                sum += item.getQuantity()*item.getPrice();
            }
            else{                
                int num=item.getPromo().getNumer();
                int denom=item.getPromo().getDenom();
                sum += (num*(item.getQuantity()/denom) + item.getQuantity()%denom)*item.getPrice();                
            }             
        }
        System.out.println("Cost Of Ur Basket:" + sum);        
        return sum;       
    }
      
}
