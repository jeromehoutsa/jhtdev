/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shopping.basket;

/**
 *
 * @author Jérôme HOUTSA
 */
public class Promo {
    /** Le nombre d'articles factures  */
    private int numer;
    
    /** Le nombre d'articles pris  */
    private int denom;

    public Promo(int numer, int denom) {
        this.numer = numer;
        this.denom = denom;
    }
    

    public int getNumer() {
        return numer;
    }

    public void setNumer(int numer) {
        this.numer = numer;
    }

    public int getDenom() {
        return denom;
    }

    public void setDenom(int denom) {
        this.denom = denom;
    }
    
}
