#######################################
##
##  README FILE - NOTICE
##  Author - Jerome HOUTSA TEMGOUA
#######################################

I°) Description

Le programme ci-joint permet de calculer la valeur d'un panier de courses tel que décrit dans l'énoncé.
Nous avons tenu compte de la possibilité de créer des offres promotionnelles pour chaque produit acheté.


II°)Pré-requis

Avoir installé les utilaires GIT 2.3 et MAVEN 3


III°)Usage

1°) Télécharger et installer le dossier du projet dans un répertoire de votre choix via GIT

2°) Lancer la commande MVN TEST pour lancer la compilation et la suite de tests unitaires


