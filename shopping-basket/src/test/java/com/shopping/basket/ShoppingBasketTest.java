package com.shopping.basket;

import java.util.Map;
import javax.jws.soap.InitParam;
import jdk.nashorn.internal.objects.annotations.Setter;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ShoppingBasketTest {
    
    ShoppingBasket shopping = new ShoppingBasket();
    final Map<String, ShoppingItem> basket= shopping.getBasket();
    
    private void setupTestOne(){      
        
        ShoppingItem apple = new ShoppingItem("Apple", 0.20, 4);        
        ShoppingItem orange = new ShoppingItem("Orange", 0.50, 3);
        ShoppingItem melon= new ShoppingItem("Water Melon", 0.80, 5);
        
        //Setting  promotions offers
        apple.setPromo(new Promo(1,2));
        melon.setPromo(new Promo(2,3));
        
        basket.put("apple", apple);
        basket.put("orange", orange);
        basket.put("waterMelon", melon);                         
    }
    
    private void setupTestTwo(){ 
          
        ShoppingItem apple = new ShoppingItem("Apple", 0.20, 4);        
        ShoppingItem orange = new ShoppingItem("Orange", 0.50, 3);
        ShoppingItem melon= new ShoppingItem("Water Melon", 0.80, 8);
        
        //Setting  promotions offers
        apple.setPromo(new Promo(1,2));
        melon.setPromo(new Promo(2,3));
        
        basket.clear();
        basket.put("apple", apple);
        basket.put("orange", orange);
        basket.put("waterMelon", melon); 
                
    }
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testCaseOne()  {
        setupTestOne();
        Assert.assertEquals("Le resultat du TestOne est K.O", new Float(5.1), shopping.calculeBasket(basket));
        
    }
    
    
    @Test
    public void testCaseTwo(){
        setupTestTwo();
        Assert.assertEquals("Le resultat du TestTwo est K.O.", new Float(6.7), shopping.calculeBasket(basket));
    }      

}
